<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Report;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function index()
    {
        try {
            $data = Report::where('is_active', true)->get();
            return $this->responsesuccess("get", $data);
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'report_number' => 'required|string|max:255|unique:reports',
                'reporter_id' => 'required|exists:users,id',
                'step_sequence' => 'required|exists:steps,sequence',
                'violation_type' => 'required|exists:categories,id',
                'estimated_loss' => 'nullable|numeric',
                'involved_parties' => 'required|string',
                'violation_location' => 'required|string',
                'violation_time' => 'required|date',
                'violation_chronology' => 'required|string',
                'has_occured_before' => 'required|in:Ya,Tidak,Tidak tahu',
                'has_been_reported' => 'required|string',
                'is_active' => 'boolean',
            ]);

            $report = Report::create([
                'report_number' => $request->report_number,
                'reporter_id' => $request->reporter_id,
                'step_sequence' => $request->step_sequence,
                'violation_type' => $request->violation_type,
                'estimated_loss' => $request->estimated_loss,
                'involved_parties' => $request->involved_parties,
                'violation_location' => $request->violation_location,
                'violation_time' => $request->violation_time,
                'violation_chronology' => $request->violation_chronology,
                'has_occured_before' => $request->has_occured_before,
                'has_been_reported' => $request->has_been_reported,
                'is_active' => $request->is_active ?? true,
                'created_by' => 'system',
            ]);

            return $this->responsesuccess("created", $report);
        } catch (\Exception  $e) {
            return $this->responsefail($e);
        }
    }

    public function show($id)
    {
        try {
            $report = Report::findOrFail($id);
            return $this->responsesuccess("get", $report);
        } catch (\Exception  $e) {
            return $this->responsefail($e);
        }
    }

    public function update(Request $request)
    {
        try {
            $report = Report::findOrFail($request->id);

            $request->validate([
                'report_number' => 'required|string|max:255|unique:reports,report_number,' . $report->id,
                'reporter_id' => 'required|exists:users,id',
                'step_sequence' => 'required|exists:steps,sequence',
                'violation_type' => 'required|exists:categories,id',
                'estimated_loss' => 'nullable|numeric',
                'involved_parties' => 'required|string',
                'violation_location' => 'required|string',
                'violation_time' => 'required|date',
                'violation_chronology' => 'required|string',
                'has_occured_before' => 'required|in:Ya,Tidak,Tidak tahu',
                'has_been_reported' => 'required|string',
                'is_active' => 'boolean',
            ]);

            $report->update([
                'report_number' => $request->report_number,
                'reporter_id' => $request->reporter_id,
                'step_sequence' => $request->step_sequence,
                'violation_type' => $request->violation_type,
                'estimated_loss' => $request->estimated_loss,
                'involved_parties' => $request->involved_parties,
                'violation_location' => $request->violation_location,
                'violation_time' => $request->violation_time,
                'violation_chronology' => $request->violation_chronology,
                'has_occured_before' => $request->has_occured_before,
                'has_been_reported' => $request->has_been_reported,
                'is_active' => $request->is_active ?? true,
                'updated_by' => $request->user()->role->name,
            ]);

            return $this->responsesuccess("update", $report);
        } catch (\Exception  $e) {
            return $this->responsefail($e);
        }
    }

    public function inactive(Request $request)
    {
        try {
            $report = Report::findOrFail($request->id);

            $report->update([
                'is_active' => $request->is_active ?? false,
                'updated_by' => $request->user()->role->name,
            ]);

            return $this->responsesuccess("update", $report);
        } catch (\Exception  $e) {
            return $this->responsefail($e);
        }
    }

    public function destroy($id)
    {
        try {
            $report = Report::findOrFail($id);
            $report->delete();

            return response()->json(null, 204);
        } catch (\Exception  $e) {
            return $this->responsefail($e);
        }
    }
}
