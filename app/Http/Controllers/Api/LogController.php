<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Log;
use Illuminate\Http\Request;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $data = Log::all();
            return $this->responsesuccess("get", $data);
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'activity_description' => 'required|string|max:255',
            ]);

            $log = Log::create([
                'activity_description' => $request->activity_description,
                'created_by' => 'system', // atau bisa diambil dari user yang login
            ]);

            return $this->responsesuccess("created", $log);
        } catch (\Exception  $e) {
            return $this->responsefail($e);
        }
    }

    public function show($id)
    {
        try {
            $log = Log::findOrFail($id);
            return $this->responsesuccess("get", $log);
        } catch (\Exception  $e) {
            return $this->responsefail($e);
        }
    }

    public function update(Request $request)
    {
        try {
            $log = Log::findOrFail($request->id);

            $request->validate([
                'activity_description' => 'required|string|max:255',
            ]);

            $log->update([
                'activity_description' => $request->activity_description,
                'updated_by' => $request->user()->role->name, // atau bisa diambil dari user yang login
            ]);

            return $this->responsesuccess("update", $log);
        } catch (\Exception  $e) {
            return $this->responsefail($e);
        }
    }

    public function destroy($id)
    {
        try {
            $log = Log::findOrFail($id);
            $log->delete();

            return response()->json(null, 204);
        } catch (\Exception  $e) {
            return $this->responsefail($e);
        }
    }
}
