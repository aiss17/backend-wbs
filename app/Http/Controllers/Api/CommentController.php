<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index()
    {
        try {
            $data = Comment::where('is_active', true)->get();
            return $this->responsesuccess("get", $data);
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'report_id' => 'required|exists:reports,id',
                'user_id' => 'required|exists:users,id',
                'comment' => 'required|string',
                'is_active' => 'boolean',
            ]);

            $comment = Comment::create([
                'report_id' => $request->report_id,
                'user_id' => $request->user_id,
                'comment' => $request->comment,
                'is_active' => $request->is_active ?? true,
                'created_by' => 'system', // atau bisa diambil dari user yang login
            ]);

            return $this->responsesuccess("created", $comment);
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function show($id)
    {
        try {
            $comment = Comment::where('id', $id)->where('is_active', true)->firstOrFail();
            return $this->responsesuccess("get", $comment);
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function update(Request $request)
    {
        try {
            $comment = Comment::where('id', $request->id)->where('is_active', true)->firstOrFail();

            $request->validate([
                'report_id' => 'required|exists:reports,id',
                'user_id' => 'required|exists:users,id',
                'comment' => 'required|string',
                'is_active' => 'boolean',
            ]);

            $comment->update([
                'report_id' => $request->report_id,
                'user_id' => $request->user_id,
                'comment' => $request->comment,
                'is_active' => $request->is_active ?? true,
                'updated_by' => $request->user()->role->name, // atau bisa diambil dari user yang login
            ]);

            return $this->responsesuccess("update", $comment);
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function inactive(Request $request)
    {
        try {
            $comment = Comment::where('id', $request->id)->where('is_active', true)->firstOrFail();

            $comment->update([
                'is_active' => $request->is_active ?? false,
                'updated_by' => $request->user()->role->name, // atau bisa diambil dari user yang login
            ]);

            return $this->responsesuccess("update", $comment);
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function destroy($id)
    {
        try {
            $comment = Comment::where('id', $id)->firstOrFail();
            $comment->delete();

            return response()->json(null, 204);
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }
}
