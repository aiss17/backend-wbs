<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Auth\Events\Registered;
// use Symfony\Component\HttpKernel\Exception\HttpException;

class UserController extends Controller
{
    use HasApiTokens;

    public function register(Request $request)
    {
        try {
            //code...
            $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:8',
                'phone' => 'required|string|max:15|unique:users',
            ]);

            $user = User::create([
                'role_id' => $request->role_id,
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => Hash::make($request->password),
                'email_verified_at' => null,
                'remember_token' => Str::random(10),
            ]);

            $token = $user->createToken('LaravelSanctumAuth')->plainTextToken;

            // Send email verification link
            // Mail::send('emails.verify', ['user' => $user], function ($message) use ($user) {
            //     $message->to($user->email);
            //     $message->subject('Email Verification');
            // });
            // event(new Registered($user));

            // return response()->json(['token' => $token], 200);
            $res = [
                'token' => $token
            ];

            return $this->responsesuccess("created", $res);
        } catch (\Exception  $e) {
            return $this->responsefail($e);
        }
    }

    public function login(Request $request)
    {
        try {
            //code...
            $request->validate([
                'email' => 'required|string|email',
                'password' => 'required|string',
            ]);

            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $user = Auth::user();
                // if (!$user->email_verified_at) {
                //     return response()->json(['error' => 'Email not verified'], 401);
                // }
                $token = $user->createToken('LaravelSanctumAuth')->plainTextToken;
                $user->token = $token;

                return $this->responsesuccess("login", $user);
            } else {
                return $this->responsesuccess("login_failed", null);
            }
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function verifyEmail($id)
    {
        $user = User::findOrFail($id);
        $user->email_verified_at = now();
        $user->save();

        return redirect()->to('/');
    }

    public function logout(Request $request)
    {
        try {
            //code...
            $request->user()->tokens()->delete();

            return $this->responsesuccess("logout", null);
            // return response()->json(['message' => 'Successfully logged out'], 200);
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->stateless()->redirect();
    }

    public function handleGoogleCallback()
    {
        try {
            $googleUser = Socialite::driver('google')->stateless()->user();
            $user = User::where('email', $googleUser->email)->first();

            if (!$user) {
                $user = User::create([
                    'name' => $googleUser->name,
                    'email' => $googleUser->email,
                    'password' => Hash::make(Str::random(24)), // Create a random password
                    'email_verified_at' => now(),
                ]);
            }

            $token = $user->createToken('LaravelSanctumAuth')->plainTextToken;

            return response()->json(['token' => $token], 200);
        } catch (Exception $e) {
            return response()->json(['error' => 'Unable to login with Google'], 500);
        }
    }
}
