<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        try {
            $data = Category::where('is_active', true)->get();
            return $this->responsesuccess("get", $data);
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'name' => 'required|string|max:255|unique:categories',
                'sequence' => 'required|string|max:255|unique:categories',
                'is_active' => 'boolean',
            ]);

            $category = Category::create([
                'name' => $request->name,
                'sequence' => $request->sequence,
                'is_active' => $request->is_active ?? true,
                'created_by' => 'system', // atau bisa diambil dari user yang login
            ]);

            return $this->responsesuccess("created", $category);
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function show($id)
    {
        try {
            $category = Category::where('id', $id)->where('is_active', true)->firstOrFail();
            return $this->responsesuccess("get", $category);
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function update(Request $request)
    {
        try {
            $category = Category::where('id', $request->id)->firstOrFail();

            $request->validate([
                'name' => 'required|string|max:255|unique:categories,name,' . $category->id,
                'sequence' => 'required|string|max:255|unique:categories,sequence,' . $category->id,
                'is_active' => 'boolean',
            ]);

            $category->update([
                'name' => $request->name,
                'sequence' => $request->sequence,
                'is_active' => $request->is_active ?? true,
                'updated_by' => $request->user()->role->name, // atau bisa diambil dari user yang login
            ]);

            return $this->responsesuccess("update", $category);
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function inactive(Request $request)
    {
        try {
            $category = Category::where('id', $request->id)->where('is_active', true)->firstOrFail();

            $category->update([
                'is_active' => $request->is_active ?? false,
                'updated_by' => $request->user()->role->name, // atau bisa diambil dari user yang login
            ]);

            return $this->responsesuccess("update", $category);
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function destroy($id)
    {
        try {
            $category = Category::where('id', $id)->firstOrFail();
            $category->delete();

            return response()->json(null, 204);
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }
}
