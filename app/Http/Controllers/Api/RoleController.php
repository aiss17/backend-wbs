<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index()
    {
        try {
            //code...
            $data = Role::where('is_active', true)->get();
            return $this->responsesuccess("get", $data);
        } catch (\Exception $e) {
            return $this->responsefail($e);
        }
    }

    public function store(Request $request)
    {
        try {
            //code...
            $request->validate([
                'name' => 'required|string|max:255|unique:roles',
                'is_active' => 'boolean',
            ]);

            $role = Role::create([
                'name' => $request->name,
                'is_active' => $request->is_active ?? true,
                'created_by' => 'system',
            ]);

            return $this->responsesuccess("created", $role);
        } catch (\Exception  $e) {
            return $this->responsefail($e);
        }
    }

    public function show($id)
    {
        try {
            //code...
            $role = Role::findOrFail($id);
            return $this->responsesuccess("get", $role);
        } catch (\Exception  $e) {
            return $this->responsefail($e);
        }
    }

    public function update(Request $request)
    {
        try {
            //code...
            $role = Role::findOrFail($request->id);

            $request->validate([
                'name' => 'required|string|max:255|unique:roles',
                'is_active' => 'boolean',
            ]);

            $role->update([
                'name' => $request->name,
                'is_active' => $request->is_active ?? true,
                'updated_by' => $request->user()->role->name,
            ]);

            return $this->responsesuccess("update", $role);
        } catch (\Exception  $e) {
            return $this->responsefail($e);
        }
    }

    public function inactive(Request $request)
    {
        try {
            //code...
            $role = Role::findOrFail($request->id);

            $role->update([
                'is_active' => $request->is_active ?? false,
                'updated_by' => $request->user()->role->name,
            ]);

            return $this->responsesuccess("update", $role);
        } catch (\Exception  $e) {
            return $this->responsefail($e);
        }
    }

    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        return response()->json(null, 204);
    }
}
