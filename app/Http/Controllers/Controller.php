<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Symfony\Component\ErrorHandler\Exception\FlattenException;

use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function responsesuccess($type, $response)
    {
      if ($type === "created") {
        return response()->json(['status' => "Success", 'message' => "Data successfully created", 'data' => $response], 201);
      } else if ($type === "get") {
        return response()->json(['status' => "Success", 'message' => "Data successfully fetched", 'data' => $response], 200);
      } else if ($type === "update") {
        return response()->json(['status' => "Success", 'message' => "Data successfully updated", 'data' => $response], 200);
      } else if ($type === "login") {
        return response()->json(['status' => "Success", 'message' => "Login successful", 'data' => $response], 200);
      } else if ($type === "logout") {
        return response()->json(['status' => "Success", 'message' => "Logout successful"], 200);
      } else if ($type === "login_failed") {
        return response()->json(['status' => "Failed", 'message' => "Login unsuccessful, unauthorized"], 401);
      }
    }

    public function responsefail($e)
    {
        $err = FlattenException::create($e);
        return response()->json(['status' => "error", 'message' => $e->getMessage()." on line: ".$err->getLine()], $err->getStatusCode());
    }

}
