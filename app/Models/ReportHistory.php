<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'report_number', 'step_sequence', 'status_sequence', 'is_active', 'created_by', 'updated_by'
    ];

    public function report()
    {
        return $this->belongsTo(Report::class, 'report_number');
    }

    public function step()
    {
        return $this->belongsTo(Step::class, 'step_sequence');
    }
}
