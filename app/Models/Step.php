<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'sequence', 'is_active', 'created_by', 'updated_by'
    ];

    public function reports()
    {
        return $this->hasMany(Report::class, 'step_sequence');
    }

    public function reportHistories()
    {
        return $this->hasMany(ReportHistory::class, 'step_sequence');
    }
}
