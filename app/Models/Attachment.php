<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    use HasFactory;

    protected $fillable = [
        'report_id',
        'original_file_name',
        'file_path',
        'is_active',
        'created_by',
        'updated_by'
    ];

    public function report()
    {
        return $this->belongsTo(Report::class);
    }
}
