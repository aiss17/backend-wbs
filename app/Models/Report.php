<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasFactory;

    protected $fillable = [
        'report_number', 'reporter_id', 'step_sequence', 'violation_type', 'estimated_loss',
        'involved_parties', 'violation_location', 'violation_time', 'violation_chronology',
        'has_occured_before', 'has_been_reported', 'is_active', 'created_by', 'updated_by'
    ];

    public function reporter()
    {
        return $this->belongsTo(User::class, 'reporter_id');
    }

    public function step()
    {
        return $this->belongsTo(Step::class, 'step_sequence');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'violation_type');
    }

    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }

    public function reportHistory()
    {
        return $this->hasMany(ReportHistory::class, 'report_number');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
