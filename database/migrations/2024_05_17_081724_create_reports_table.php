<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->id();
            $table->string('report_number')->unique();
            $table->foreignId('reporter_id')->constrained('users')->onDelete('cascade');
            $table->foreignId('step_sequence')->constrained('steps')->onDelete('cascade');
            $table->foreignId('violation_type')->constrained('categories')->onDelete('cascade');
            $table->decimal('estimated_loss', 15, 2)->nullable();
            $table->string('involved_parties');
            $table->string('violation_location');
            $table->timestamp('violation_time');
            $table->text('violation_chronology');
            $table->enum('has_occured_before', ['Ya', 'Tidak', 'Tidak tahu']);
            $table->string('has_been_reported');
            $table->boolean('is_active')->default(true);
            $table->timestamps();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('reports');
    }
};
