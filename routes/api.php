<?php

use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\ReportController;
use App\Http\Controllers\Api\LogController;
use App\Http\Controllers\Api\CommentController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\AttachmentController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('auth')->group(function () {
    Route::post('register', [UserController::class, 'register']);
    Route::post('login', [UserController::class, 'login']);
    Route::get('email/verify/{id}', [UserController::class, 'verifyEmail'])->name('verification.verify');

    // Routes for Google Login
    Route::get('login/google', [UserController::class, 'redirectToGoogle']);
    Route::get('login/google/callback', [UserController::class, 'handleGoogleCallback']);
    Route::get('user', [UserController::class, 'user'])->middleware('auth:sanctum');
    Route::post('logout', [UserController::class, 'logout'])->middleware('auth:sanctum');
});

Route::prefix('role')->middleware('auth:sanctum')->group(function () {
    Route::post('insert', [RoleController::class, 'store']);
    Route::get('getAll', [RoleController::class, 'index']);
    Route::get('getById/{id}', [RoleController::class, 'show']);
    Route::post('update', [RoleController::class, 'update']);
    Route::post('inactive', [RoleController::class, 'inactive']);
    Route::post('delete', [RoleController::class, 'destroy']);
});

Route::prefix('report')->middleware('auth:sanctum')->group(function () {
    Route::post('insert', [ReportController::class, 'store']);
    Route::get('getAll', [ReportController::class, 'index']);
    Route::get('getById/{id}', [ReportController::class, 'show']);
    Route::post('update', [ReportController::class, 'update']);
    Route::post('inactive', [ReportController::class, 'inactive']);
    Route::post('delete', [ReportController::class, 'destroy']);
});

Route::prefix('log')->middleware('auth:sanctum')->group(function () {
    Route::post('insert', [LogController::class, 'store']);
    Route::get('getAll', [LogController::class, 'index']);
    Route::get('getById/{id}', [LogController::class, 'show']);
    Route::post('update', [LogController::class, 'update']);
    Route::post('inactive', [LogController::class, 'inactive']);
    Route::post('delete', [LogController::class, 'destroy']);
});

Route::prefix('comment')->middleware('auth:sanctum')->group(function () {
    Route::post('insert', [CommentController::class, 'store']);
    Route::get('getAll', [CommentController::class, 'index']);
    Route::get('getById/{id}', [CommentController::class, 'show']);
    Route::post('update', [CommentController::class, 'update']);
    Route::post('inactive', [CommentController::class, 'inactive']);
    Route::post('delete', [CommentController::class, 'destroy']);
});

Route::prefix('category')->middleware('auth:sanctum')->group(function () {
    Route::post('insert', [CategoryController::class, 'store']);
    Route::get('getAll', [CategoryController::class, 'index']);
    Route::get('getById/{id}', [CategoryController::class, 'show']);
    Route::post('update', [CategoryController::class, 'update']);
    Route::post('inactive', [CategoryController::class, 'inactive']);
    Route::post('delete', [CategoryController::class, 'destroy']);
});

Route::prefix('attachment')->middleware('auth:sanctum')->group(function () {
    Route::post('insert', [AttachmentController::class, 'store']);
    Route::get('getAll', [AttachmentController::class, 'index']);
    Route::get('getById/{id}', [AttachmentController::class, 'show']);
    Route::post('update', [AttachmentController::class, 'update']);
    Route::post('inactive', [AttachmentController::class, 'inactive']);
    Route::post('delete', [AttachmentController::class, 'destroy']);
});
